use std::collections::HashMap;
use std::collections::hash_map::Entry::{Occupied, Vacant};
use hyper::server::{Handler, Request, Response};
use hyper::uri::RequestUri;

#[derive(Clone)]
enum RouteTree {
    Node(HashMap<String, RouteTree>),
    Leaf
}

enum Methods {
    Get,
    Post,
    Put,
    Delete,
    Head,
    Options
}

pub struct Routes {
    route_map: HashMap<String, RouteTree>
}

impl Routes {

    pub fn new () -> Routes {
        Routes {
            route_map: HashMap::new()
        }
    }

    //TODO: http methods are completely broken right now. this shouldn't be TOO difficult but I
    //need to punt on it
    pub fn get <F: Fn(Request, Response) -> ()> (&mut self, path: Vec<PathParts>, callback: F) -> &mut Routes {
        self.register_route(Methods::Get, path, callback)
    }

    pub fn post <F: Fn(Request, Response) -> ()> (&mut self, path: Vec<PathParts>, callback: F) -> &mut Routes {
        self.register_route(Methods::Post, path, callback)
    }

    fn register_route <F: Fn(Request, Response) -> () > (&mut self, method: Methods, path: Vec<PathParts>, callback: F) -> &mut Routes {
        {
            let mut initial_map = &mut self.route_map;

            path.iter().enumerate().fold(Traverse::Continue(initial_map), |o_map, e| {
                match o_map {
                    Traverse::Continue(map) => {
                        let (i, part) = e;
                        //TODO: there should be no reason why the HashMap cannot be keyed by
                        //PathParts... but rustc keeps yelling at me. will come back later
                        let key = match *part {
                            PathParts::StrVal(ref p) => p.clone()  + ":strvar",
                            PathParts::FloatVar => "floatvar".to_string(),
                            PathParts::NumVar => "numvar".to_string(),
                            PathParts::StrVar => "strvar".to_string()
                        };

                        let new = map.entry(key).or_insert_with(|| {
                            if (i+1 == path.len()) {
                                RouteTree::Leaf
                            } else {
                                RouteTree::Node(HashMap::new())
                            }
                        });

                        match new {
                            &mut RouteTree::Node(ref mut hm) => Traverse::Continue(hm),
                            _ => Traverse::End
                        }
                    },
                    none => none
                }
            });
        }
        self
    }

    fn handle_request(&self, req: Request, res: Response) {
        let method = req.method;
        match req.uri {
            RequestUri::AbsolutePath(path) => {
                let something = &self.route_map;
                let mut sss = something;

                for u in path.split("/").skip_while(|x| *x == "") {

                    match sss.get(u) {
                        Some(a) => {
                            
                        },
                        None => return
                    };
                    println!("{}", u);
                }
            },
            _ => {();}
        };
    }
}

impl Handler for Routes {
    fn handle(&self, req: Request, res: Response) {
        self.handle_request(req, res);
    }
}

pub enum Traverse <'a> {
    Continue (&'a mut HashMap<String, RouteTree>),
    End
}

#[derive(Hash, Eq, PartialEq, Clone)]
pub enum PathParts {
    StrVal(String),
    FloatVar,
    NumVar,
    StrVar
}

///Wrapper over a string slice. This exists to future proof for backwards compatibility in case
///a DSL is intoduced (not sure if such a thing is even possible in Rust, but worth exploring
///imho. At the very least it would eliminate the : prefix character, making path matching complete
pub fn path (str_path: &str) -> Vec<PathParts> {
    extract_path_parts(str_path)
}

fn extract_path_parts(path: &str) -> Vec<PathParts> {
    let segments = path.split("/");
    let fsegment = segments.skip_while(|x| *x == "");
    let ret = fsegment.map(|seg| -> PathParts {
        println!("{}", seg);
        match seg {
            _ if seg.starts_with(":") => PathParts::StrVar,
            _ => PathParts::StrVal(seg.to_string())
        }
    });
    ret.collect()
}
