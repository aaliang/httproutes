mod routes;

extern crate hyper;

use hyper::Server;
use hyper::server::{Request, Response};
use hyper::net::Fresh;

use std::io::Write;

use std::sync::mpsc::channel;
use std::thread;

fn handle_request (req: Request, resp: Response<Fresh>) {
    println!("method: {}", req.method);
    println!("uri: {}", req.uri);
    resp.send(b"Hello World").unwrap();
}

fn main() {
    let mut r = routes::Routes::new();

    r.get(routes::path("/edit/a"), |req: Request, resp: Response<Fresh>| {
        
    });

    Server::http("127.0.0.1:3000").unwrap().handle(r);
}

/*fn main() {

    let (tx, rx) = channel();

    thread::spawn(move|| {
        tx.send("hello_world").unwrap();
    });

    println!("{:?}", rx.recv().unwrap());
}*/
